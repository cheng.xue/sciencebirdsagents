import argparse
import logging
import os
import random
import time
import warnings
from pathlib import Path

import numpy as np
import torch

from HeuristicAgents.CollectionAgent import CollectionAgent
from HeuristicAgents.CollectionAgentThread import MultiThreadTrajCollection
from SBEnviornment.SBEnvironmentWrapper import SBEnvironmentWrapper
from Utils.Config import config
from Utils.Parameters import Parameters

warnings.filterwarnings('ignore')

# Set a seed value
seed_value = 5123690
os.environ['PYTHONHASHSEED'] = str(seed_value)
random.seed(seed_value)
np.random.seed(seed_value)
torch.manual_seed(seed_value)

logger = logging.getLogger("Main Training and Testing")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def generate_degree_to_shoot(agent_id, n=10):
    degrees = [i for i in range(10, 161, 10)]
    start_ind = degrees[agent_id]
    end_ind = degrees[agent_id+1]
    return list(range(start_ind,end_ind))


capability_templates_dict = {
    '1_01': ['1_01_01', '1_01_02', '1_01_03'],
    '1_02': ['1_02_01', '1_02_03', '1_02_04', '1_02_05', '1_02_06'],
    '2_01': ['2_01_01', '2_01_02', '2_01_03', '2_01_04', '2_01_05', '2_01_06', '2_01_07', '2_01_08', '2_01_09'],
    '2_02': ['2_02_01', '2_02_02', '2_02_03', '2_02_04', '2_02_05', '2_02_06', '2_02_07', '2_02_08'],
    '2_03': ['2_03_01', '2_03_02', '2_03_03', '2_03_04', '2_03_05'],
    '2_04': ['2_04_04', '2_04_05', '2_04_06', '2_04_02', '2_04_03'],
    '3_01': ['3_01_01', '3_01_02', '3_01_03', '3_01_04', '3_01_06'],
    '3_02': ['3_02_01', '3_02_02', '3_02_03', '3_02_04'],
    '3_03': ['3_03_01', '3_03_02', '3_03_03', '3_03_04'],
    '3_04': ['3_04_01', '3_04_02', '3_04_03', '3_04_04'],
    '3_05': ['3_05_03', '3_05_04', '3_05_05'],
    '3_06': ['3_06_01', '3_06_04', '3_06_06', '3_06_03', '3_06_05'],
    '3_07': ['3_07_01', '3_07_02', '3_07_03', '3_07_04', '3_07_05'],
    '3_08': ['3_08_01', '3_08_02'],
    '3_09': ['3_09_01', '3_09_02', '3_09_03', '3_09_04', '3_09_07', '3_09_08']}

full_strength = ['1_01_01', '1_01_02', '1_01_03','1_02_01', '1_02_03', '1_02_04','2_01_01', '2_01_02', '2_01_03',
                 '2_01_04','2_02_01', '2_02_02', '2_02_03', '2_02_04', '2_02_05', '2_03_01', '2_03_02', '2_03_03',
                 '2_04_01', '2_04_02', '2_04_03', '3_01_03', '3_01_04', '3_02_01', '3_02_02', '3_02_03', '3_02_04',
                 '3_03_01', '3_03_02', '3_03_03', '3_03_04', '3_04_01', '3_04_02', '3_04_03', '3_04_04',
                  '3_06_01', '3_06_04', '3_06_06', '3_06_03', '3_06_05',
                 ]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--template', metavar='N', type=str)
    parser.add_argument('--level_path', type=str, default='fourth generation')
    parser.add_argument('--game_version', type=str, default='Linux')
    args = parser.parse_args()
    memory_saving_path = os.path.join('LearningAgents', 'saved_memory')
    if len(args.template.split("_")) == 3:
        param = Parameters([args.template], level_path=args.level_path,
                           game_version=args.game_version)
        c = config(**param.param)
        param_name = args.template
    param_name = param_name + "_" + "_" + args.level_path + "_" + args.game_version + "_collected"
    logger.info('collecting sample for template {}'.format(args.template))
    level_winning_rate = {i: 0 for i in c.train_level_list}
    start_time = time.time()
    for game_level_idx in c.train_level_list:
        logger.info("start collecting template {} level {}...".format(args.template, game_level_idx))
        degree_agents = []
        random_agents = []
        if args.template in full_strength:
            for i in range(15):
                env = SBEnvironmentWrapper(reward_type=c.reward_type, speed=c.simulation_speed,
                                           game_version=args.game_version, if_head=True)
                degrees = generate_degree_to_shoot(agent_id=i, n=10)  # generate 10 degrees to shoot
                degree_agent = CollectionAgent(env=env, agent_id=i, degrees_to_shoot=degrees, game_level_idx=game_level_idx,
                                               template=args.template, mode='Degree')
                degree_agents.append(degree_agent)
            if len(degree_agents) != 0:
                am = MultiThreadTrajCollection(degree_agents)
                am.connect_and_run_agents()
                env.close()
                time.sleep(5)

            for i in range(15):

                env = SBEnvironmentWrapper(reward_type=c.reward_type, speed=c.simulation_speed,
                                           game_version=args.game_version, if_head=True)
                random_agent = CollectionAgent(env=env, agent_id=i, game_level_idx=game_level_idx,
                                               template=args.template, mode='Degree+Random')
                random_agents.append(random_agent)
            am = MultiThreadTrajCollection(random_agents)
            am.connect_and_run_agents()
            env.close()
            time.sleep(5)

        else:
            for i in range(15):
                env = SBEnvironmentWrapper(reward_type=c.reward_type, speed=c.simulation_speed,
                                           game_version=args.game_version, if_head=True)
                random_agent = CollectionAgent(env=env, agent_id=i, game_level_idx=game_level_idx,
                                               template=args.template, mode='Random')
                random_agents.append(random_agent)
            am = MultiThreadTrajCollection(random_agents)
            am.connect_and_run_agents()
            env.close()
            time.sleep(5)

        logger.info("finished collecting template {} level {}...".format(args.template, game_level_idx))
        end_time = time.time()
        logger.info("running time: {:.2f} mins".format((end_time - start_time) / 60))

    logger.info('collecting done')
    logger.info("total running time: {:.2f} mins".format((end_time - start_time) / 60))
