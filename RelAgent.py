from collections import deque

import numpy as np
import torch
import torch.nn as nn
import torchvision.transforms as T
import random

from LearningAgents.LearningAgent import LearningAgent
from Utils.LevelSelection import LevelSelectionSchema
from LearningAgents.Memory import ReplayMemory
from SBEnviornment.SBEnvironmentWrapper import SBEnvironmentWrapper
from torch.utils.tensorboard import SummaryWriter
from einops import rearrange, reduce


class MultiHeadRelationalModule(nn.Module):
    def __init__(self):
        super(MultiHeadRelationalModule, self).__init__()
        self.conv1_ch = 16
        self.conv2_ch = 20 #dim `F` in paper
        self.conv3_ch = 24
        self.conv4_ch = 30
        self.H = 7
        self.W = 7
        self.node_size = 64 # entity embedding size
        self.lin_hid = 100
        self.out_dim = 180 # actions
        self.ch_in = 3
        self.sp_coord_dim = 2
        self.N = int(self.H * self.W)
        self.n_heads = 3

        self.conv1 = nn.Conv2d(self.ch_in,self.conv1_ch,kernel_size=(1,1),padding=0)
        self.conv2 = nn.Conv2d(self.conv1_ch,self.conv2_ch,kernel_size=(1,1),padding=0)

        self.proj_shape = (self.conv2_ch+self.sp_coord_dim,self.n_heads * self.node_size)

        # Multihead attention
        self.k_proj = nn.Linear(*self.proj_shape)
        self.q_proj = nn.Linear(*self.proj_shape)
        self.v_proj = nn.Linear(*self.proj_shape)

        self.k_lin = nn.Linear(self.node_size,self.N)
        self.q_lin = nn.Linear(self.node_size,self.N)
        self.a_lin = nn.Linear(self.N,self.N)

        self.node_shape = (self.n_heads, self.N,self.node_size)
        self.k_norm = nn.LayerNorm(self.node_shape, elementwise_affine=True)
        self.q_norm = nn.LayerNorm(self.node_shape, elementwise_affine=True)
        self.v_norm = nn.LayerNorm(self.node_shape, elementwise_affine=True)

        self.linear1 = nn.Linear(self.n_heads * self.node_size, self.node_size)
        self.norm1 = nn.LayerNorm([self.N,self.node_size], elementwise_affine=False)
        self.linear2 = nn.Linear(self.node_size, self.out_dim)

    def forward(self,x):

        N, Cin, H, W = x.shape
        x = self.conv1(x)
        x = torch.relu(x)
        x = self.conv2(x)
        x = torch.relu(x)

        with torch.no_grad():
            self.conv_map = x.cpu().clone()
            _,_,cH,cW = x.shape
            xcoords = torch.arange(cW).repeat(cH,1).float() / cW
            ycoords = torch.arange(cH).repeat(cW,1).transpose(1,0).float() / cH
            spatial_coords = torch.stack([xcoords,ycoords],dim=0)
            spatial_coords = spatial_coords.unsqueeze(dim=0)
            spatial_coords = spatial_coords.repeat(N,1,1,1)
        x = torch.cat([x,spatial_coords],dim=1)
        x = x.permute(0,2,3,1) # batch_size, H, W, C
        x = x.flatten(1,2) # batch_size, HxW, C

        # key, query, value separation
        K = rearrange(self.k_proj(x), "b n (head d) -> b head n d", head=self.n_heads)
        K = self.k_norm(K)

        Q = rearrange(self.q_proj(x), "b n (head d) -> b head n d", head=self.n_heads)
        Q = self.q_norm(Q)

        V = rearrange(self.v_proj(x), "b n (head d) -> b head n d", head=self.n_heads)
        V = self.v_norm(V)

        A = torch.nn.functional.elu(self.q_lin(Q) + self.k_lin(K))
        A = self.a_lin(A)
        A = torch.nn.functional.softmax(A,dim=3)
        with torch.no_grad():
            self.att_map = A.cpu().clone()
        E = torch.einsum('bhfc,bhcd->bhfd',A,V)

        #collapse head dimension
        E = rearrange(E, 'b head n d -> b n (head d)')
        # B N D' . D' D -> B N D
        E = self.linear1(E)
        E = torch.relu(E)
        E = self.norm1(E)
        # B N D -> B D
        E = E.max(dim=1)[0]
        y = self.linear2(E)
        y = torch.nn.functional.elu(y)
        return y

    def transform(self, state):
        t = T.Compose([T.ToPILImage(), T.Resize((self.H, self.W)),
                       T.ToTensor(), T.Normalize((.5, .5, .5), (.5, .5, .5))])
        return t(state)

class RelAgent(LearningAgent):

    def __init__(self, level_list: list,
                 replay_memory: ReplayMemory,
                 env: SBEnvironmentWrapper,
                 writer: SummaryWriter = None,
                 level_selection_function=LevelSelectionSchema.RepeatPlay(5).select,
                 id: int = 28888):
        LearningAgent.__init__(self, level_list=level_list, env=env, id=id, replay_memory=replay_memory, writer=writer)

        self.level_list = level_list
        self.env = env
        self.writer = writer
        self.level_selection_function = level_selection_function
        self.id = id
        self.replay_memory = replay_memory
        self.action_type = 'discrete'
        self.state_representation_type = 'image'

        self.network = MultiHeadRelationalModule()
        self.target_network = MultiHeadRelationalModule()

    def get_minibatch(self, replay,size):
        """
        Returns a random subset of experiences from the experience replay memory
        """
        batch_ids = np.random.randint(0,len(replay),size)
        batch = [replay[x] for x in batch_ids] #list of tuples
        state_batch = torch.cat([s for (s,a,r,s2,d) in batch],)
        action_batch = torch.Tensor([a for (s,a,r,s2,d) in batch]).long()
        reward_batch = torch.Tensor([r for (s,a,r,s2,d) in batch])
        state2_batch = torch.cat([s2 for (s,a,r,s2,d) in batch],dim=0)
        done_batch = torch.Tensor([d for (s,a,r,s2,d) in batch])
        return state_batch,action_batch,reward_batch,state2_batch, done_batch

    def get_qtarget(self, qvals,r,df,done):
        """
        `qvals`: q value batch Nx7
        `r`: reward batch Nx1
        `df`: discount factor, scalar float
        """
        maxqvals = torch.max(qvals,dim=1)[0]
        targets = r + (1-done) * df * maxqvals
        return targets

    def get_qtarget_ddqn(self, qvals,r,df,done):
        """
        `qvals`: q value batch Nx7
        `r`: reward batch Nx1
        `df`: discount factor, scalar float
        """
        targets = r + (1-done) * df * qvals
        return targets

    def lossfn(self, pred,targets,actions):
        """
        Computes q-value loss
        """

        loss = torch.mean(torch.pow( \
            targets.detach() - \
            pred.gather(dim=1,index=actions.unsqueeze(dim=1)).squeeze() \
            ,2),dim=0)

        return loss

    def update_replay_old(self, replay,exp,replay_size):
        r = exp[2]
        N = 1
        if r > 0:
            N = 50
        for i in range(N):
            if len(replay) < replay_size:
                replay.append(exp)
            else:
                rid = np.random.randint(0,len(replay))
                replay[rid] = exp
        return replay

    def update_replay(self, replay,exp,replay_size):
        r = exp[2]
        N = 1
        if r > 0:
            N = 50
        for i in range(N):
            replay.append(exp)
        return replay

    def softmax(self, x,tau=1.9):
        z = torch.exp(tau*x) / torch.exp(tau*x).sum()
        return z

    def logsoftmax(self, x,tau=1.9):
        z = torch.log(self.softmax(x,tau=tau))
        return z

    def degToShot(self, deg):
        # deg = torch.argmax(q_values, 1) + 90
        deg = deg + 90 if self.network.out_dim == 180 else 180
        ax_pixels = 200 * torch.cos(torch.deg2rad(deg.float())).view(-1, 1)
        ay_pixels = 200 * torch.sin(torch.deg2rad(deg.float())).view(-1, 1)
        out = torch.cat((ax_pixels, ay_pixels), 1)
        if out.size(0) == 1:
            return out[0]
        return out

    def train_model_memory(self):
        epochs = 50000
        replay_size = 9000
        batch_size = 50
        lr = 0.0005
        gamma = 0.99
        replay = deque(maxlen=replay_size) #list of tuples (s1,a,r,s2)
        opt = torch.optim.Adam(params=self.network.parameters(),lr=lr)
        losses = []
        avg_rewards = []
        state, reward, done, info = env.reset()
        state = self.network.transform(torch.from_numpy(state).float()).unsqueeze(dim=0)
        eps = 0.3
        update_freq = 100
        #
        ep_len = 0
        for i in range(epochs):
            #collect experiences
            ep_len+=1
            pred = self.network(state).cpu() #get q-values
            action = torch.argmax(pred).detach()
            # TODO: random action
            # if np.random.rand() < eps:
            #     action = float(torch.randint(0,5,size=(1,)).squeeze())

            #print(action)
            state2, reward, done, info = env.step(self.degToShot(action).to('cpu'))
            reward = -0.01 if reward == 0 else reward
            state2 = self.network.transform(torch.from_numpy(state2).float()).unsqueeze(dim=0)
            exp = (state,action,reward,state2,done)
            #
            replay = self.update_replay(replay,exp,replay_size)
            #
            if done:
                avg_rewards.append(ep_len)
                ep_len = 0
                if reward > 0:
                    print("Game won")
                else:
                    print("Game lost...")

                state, reward, done, info = env.reset()
                state = self.network.transform(torch.from_numpy(state).float()).unsqueeze(dim=0)
            else:
                state = state2
            if len(replay) > batch_size:
                #begin batch training
                opt.zero_grad()
                #
                state_batch,action_batch,reward_batch,state2_batch,done_batch = self.get_minibatch(replay,batch_size)
                #
                q_pred = self.network(state_batch).cpu()
                astar = torch.argmax(q_pred,dim=1)
                qs = self.target_network(state2_batch).cpu().gather(dim=1,index=astar.unsqueeze(dim=1)).squeeze()

                targets = self.get_qtarget_ddqn(qs.detach(),reward_batch.detach(),gamma,done_batch)
                #
                loss = self.lossfn(q_pred,targets.detach(),action_batch)
                loss.backward()
                torch.nn.utils.clip_grad_norm_(self.network.parameters(), max_norm=1.0)
                losses.append(loss.detach().numpy())

                opt.step()
            if i % update_freq == 0:
                self.target_network.load_state_dict(self.network.state_dict())

        losses = np.array(losses)
        avg_rewards = np.array(avg_rewards)

        print(avg_rewards)

if __name__ == '__main__':
    level_list = [1, 2, 3]
    replay_memory = ReplayMemory(1000)
    env = SBEnvironmentWrapper(reward_type='passing')
    agent = RelAgent(level_list=level_list, replay_memory=replay_memory, env=env)
    env.make(agent, state_representation_type='image')

    agent.train_model_memory()