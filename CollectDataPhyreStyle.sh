#!/bin/bash
mode=$1
if [ $mode = "within_template" ]; then
  templates=$(python Utils/GenerateTemplateName.py --level_path "HiphyLevels_OneBird")
else
  templates=$(python Utils/GenerateCapabilityName.py)
fi
for val in $templates; do
  echo running $val
  python GatherPhyreSample.py --template $val --game_version Linux043 --level_path "HiphyLevels_OneBird"
done
